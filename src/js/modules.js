angular.module('app.filters', []);
angular.module('app.directives', []);
angular.module('app.services', []);
angular.module('app.controllers', ['app.services']);
angular.module('app.providers', []);