angular.module('app.services')
  .factory('paginatorService', function () {
    
    function paginator ($scope) {
      
      var self = this,
          page = 0;
      
      this.totalItems = 0;
      this.totalPages = 0;
      this.itemsPerPageOptions = [10, 20, 50];
      this.itemsPerPage = this.itemsPerPageOptions.first();
      this.rangeStart = 0;
      this.rangeEnd = 0;        

      Object.defineProperty(this, 'currentPage', {
        get: function () {
          return page + 1;
        },
        set: function (newValue) {
          page = newValue - 1;
        }
      });
      
      this.update = function (totalItems) {
        updateTotalItems(totalItems);        
      };          

      this.firstPage = function () {
        page = 0;
      };
      
      this.previousPage = function () {
        if (self.currentPage > 1) {
          page--;
        }
      };
      
      this.nextPage = function () {
        if (self.currentPage < self.totalPages) {
          page++;
        }
      };
      
      this.lastPage = function () {
        page = self.totalPages - 1;
      };          
      
      function updateTotalItems(totalItems) {
        self.totalItems = totalItems;
        self.totalPages = Math.floor(totalItems / self.itemsPerPage) + (totalItems % self.itemsPerPage > 0 ? 1 : 0);
        updateRanges();
      }

      function updateRanges() {
        self.rangeStart = page * self.itemsPerPage + 1;
        self.rangeEnd = self.rangeStart + self.itemsPerPage < self.totalItems ? self.rangeStart + self.itemsPerPage : self.totalItems;
      }

      $scope.$watch(function () {
        return self.itemsPerPage;
      }, updateTotalItems);

      $scope.$watch(function () {
        return self.currentPage;
      }, updateRanges);
    }

    return paginator;
  });