angular.module('app.services')
  .service('jsonPlaceholderService', function($http) {

    var localMode = true;

    function filterDataByProperty(data, property, value) {
      return data.filter(function(x) {
        return x[property] === value;
      });
    }

    function httpGet(url, jsonFile, successCallback, errorCallback) {
      if (localMode && jsonFile) {
        $.getJSON(jsonFile, successCallback);

      } else {
        $http({
          url: url,
          method: 'GET'
        }).then(function(response) {
          successCallback(response.data);
        }, errorCallback);
      }
    }

    function httpGetFilter(url, jsonFile, property, value, successCallback, errorCallback) {
      if (localMode && jsonFile) {
        $.getJSON(jsonFile, function(data) {
          successCallback(filterDataByProperty(data, property, value));
        });

      } else {
        $http({
          url: url,
          method: 'GET'
        }).then(function(response) {
          successCallback(filterDataByProperty(response.data, property, value));
        }, errorCallback);
      }
    }

    function getAlbums(successCallback, errorCallback) {
      httpGet('http://jsonplaceholder.typicode.com/albums',
        './dist/data/albums.json',
        successCallback,
        errorCallback);
    };

    function getAlbumsByUser(userId, successCallback, errorCallback) {
      httpGetFilter('http://jsonplaceholder.typicode.com/albums',
        './dist/data/albums.json',
        'userId',
        userId,
        successCallback,
        errorCallback);
    }

    function getComments(successCallback, errorCallback) {
      httpGet('http://jsonplaceholder.typicode.com/comments',
        './dist/data/comments.json',
        successCallback,
        errorCallback);
    }

    function getCommentsByPost(postId, successCallback, errorCallback) {
      httpGetFilter('http://jsonplaceholder.typicode.com/comments',
        './dist/data/comments.json',
        'postId',
        postId,
        successCallback,
        errorCallback);
    }

function getPhotos(successCallback, errorCallback) {
      httpGet('http://jsonplaceholder.typicode.com/photos',
        null,
        successCallback,
        errorCallback);
    }

    function getPhotosByAlbum(albumId, successCallback, errorCallback) {
      httpGetFilter('http://jsonplaceholder.typicode.com/photos',
        null,
        'albumId',
        albumId,
        successCallback,
        errorCallback);
    }

    function getPosts(successCallback, errorCallback) {
      httpGet('http://jsonplaceholder.typicode.com/posts',
        './dist/data/posts.json',
        successCallback,
        errorCallback);
    }

    function getPostsByUser(userId, successCallback, errorCallback) {
      httpGetFilter('http://jsonplaceholder.typicode.com/posts',
        './dist/data/posts.json',
        'userId',
        userId,
        successCallback,
        errorCallback);
    }

    function getTodos(successCallback, errorCallback) {
      httpGet('http://jsonplaceholder.typicode.com/todos',
        './dist/data/todos.json',
        successCallback,
        errorCallback);
    }

    function getTodosByUser(userId, successCallback, errorCallback) {
      httpGetFilter('http://jsonplaceholder.typicode.com/todos',
        './dist/data/todos.json',
        'userId',
        userId,
        successCallback,
        errorCallback);
    }

    function getUsers(successCallback, errorCallback) {
      httpGet('http://jsonplaceholder.typicode.com/users',
        './dist/data/users.json',
        successCallback,
        errorCallback);
    }

    return {
      getAlbums: getAlbums,
      getAlbumsByUser: getAlbumsByUser,
      getComments: getComments,
      getCommentsByPost: getCommentsByPost,
      getPhotos: getPhotos,
      getPhotosByAlbum: getPhotosByAlbum,
      getPosts: getPosts,
      getPostsByUser: getPostsByUser,
      getTodos: getTodos,
      getTodosByUser: getTodosByUser,
      getUsers: getUsers
    };
  });