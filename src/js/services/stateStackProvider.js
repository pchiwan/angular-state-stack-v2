'use strict';

angular.module('app.providers')
  .provider('$stateStack', function() {

  return {

    $get: function() {

      var stateStack = [];

      function clear() {
        stateStack = [];
      }

      function logStack() {
        /// <summary>Prints the current stack in the console.</summary>

        var states = stateStack.map(function (state) {
          return state.name;
        }).reverse();

        console.log(states.join('<--'));
      }

      function popState() {
        /// <summary>Pop last state of the stack.</summary>

        if (stateStack.length) {
          stateStack.pop();
        }
      }

      function pushState(stateName) {
        /// <summary>Push a new state in the stack.</summary>

        // check whether current state is going further down in a state hierarchy or not
        if (stateStack.any()) {
          var lastState = stateStack.last();
          if (stateName.contains(lastState.name) || lastState.name.contains(stateName)) {
            if (stateName.length < lastState.name.length) {
              // going up in the state hierarchy
              popState();
              return;
            }
          } else {
            // we've navigated to a different state hierarchy so let's clear the stack first
            clear();
          }
        }

        // add current state to the stack
        stateStack.push({
          name: stateName,
          params: {}
        });
      }

      function recoverState(stateName) {
        /// <summary>Get the current state (last in the stack).</summary>

        return stateStack.any() && stateStack.last().name === stateName
          ? stateStack.last().params
          : null;
      }

      function updateState(stateParams) {
        /// <summary>Update parameters of current state.</summary>

         angular.extend(stateStack.last().params, stateParams);
      }

      return {
        clear: clear,
        logStack: logStack,
        popState: popState,
        pushState: pushState,
        recoverState: recoverState,
        updateState: updateState
      };
    }
  };
});