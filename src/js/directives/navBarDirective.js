angular.module('app.directives')
	.directive('navBar', function () {

		function controller ($scope) {
			var currentState = '';
			$scope.$on('$stateChangeSuccess', function(event, toState){
				currentState = toState.name;
			});
			
			$scope.isActive = function (menuEntry) {
				var levels = currentState.split('-');
				return levels.first() === menuEntry;
			};
		}

		return {
			controller: controller,
			scope: {
				navBarItems: '='	
			},
			template: 
				'<div class="nav-bar">'+
					'<a ng-repeat="item in navBarItems" ui-sref="{{item.state}}" ng-class="{ active: isActive(item.state) }">{{item.title}}</a>'+
				'</div>',
			replace: true,
			restrict: 'E'
		};
	});