angular.module('app.directives')
	.directive('searchBar', function () {

		return {
			template: 
				'<div class="search-bar">' +
					'<div class="search-bar-inner">' +
						'<input type="text" ng-model="ctrl.searchText" placeholder="search..." />' +
						'<img src="http://icons.iconarchive.com/icons/elegantthemes/beautiful-flat-one-color/128/magnifying-glass-icon.png" alt="Search" />' +
					'</div>' +
				'</div>',
			replace: true,
			restrict: 'E'
		};
	});