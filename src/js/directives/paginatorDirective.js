angular.module('app.directives')
  .directive('paginator', function () {
    
    return {
      template: 
        '<div class="paginator">' +
          'Items per page: <select ng-model="ctrl.paginator.itemsPerPage" convert-to-number>' +
            '<option ng-repeat="option in ctrl.paginator.itemsPerPageOptions" value="{{option}}" ng-bind="option"></option>' +
          '</select>' +
          ' | ' +
          '<span class="fa fa-backward" ng-click="ctrl.paginator.firstPage()"></span><span class="fa fa-step-backward" ng-click="ctrl.paginator.previousPage()"></span>' +
          'Items <span ng-bind="ctrl.paginator.rangeStart"></span> - <span ng-bind="ctrl.paginator.rangeEnd"></span> of <span ng-bind="ctrl.paginator.totalItems"></span> total' +
          '<span class="fa fa-step-forward" ng-click="ctrl.paginator.nextPage()"></span><span class="fa fa-forward" ng-click="ctrl.paginator.lastPage()"></span>' +
        '</div>',
      replace: true,
      restrict: 'E'
    };
  })