(function() {

  function baseController($scope, $rootScope, $stateParams, $injector) {

    var $stateStack = $injector.get('$stateStack'),
      $timeout = $injector.get('$timeout'),
      paginatorService = $injector.get('paginatorService');
      self = this;

    this.dataSource = [];
    this.searchText = '';
    this.loading = false;
    this.loadMethod = null;
    this.paginator = new paginatorService($scope);

    this.defaultSuccessCallback = function(data) {
      self.dataSource = data;
      $scope.$applyAsync(function () {
        self.paginator.update(self.dataSource.length);
        self.loading = false;
      });
    };

    this.load = function(params) {
      self.loading = true;
      if (self.loadMethod) {
        params = !!params ? [params] : [];
        params.push(self.defaultSuccessCallback);
        self.loadMethod.apply(this, params);
      }
    }

    $scope.$on('$stateChangeStart', function() {
      $rootScope.isLoading = true;

      // update current state's params before navigating away from it				
      $stateStack.updateState({
        searchText: self.searchText,
        currentPage: self.paginator.currentPage,
        itemsPerPage: self.paginator.itemsPerPage
      });
      $stateStack.logStack();
    });

    $scope.$on('$stateChangeSuccess', function(event, toState) {
      // push current state in the stack if it's not already there
      $stateStack.pushState(toState.name);

      // load properties of current state if it's in the stack
      var params = $stateStack.recoverState(toState.name);

      $scope.$applyAsync(function() {
        self.searchText = !!params && params.hasOwnProperty('searchText') ? params.searchText : '';
        self.paginator.currentPage = !!params && params.hasOwnProperty('currentPage') ? params.currentPage : 1;
        self.paginator.itemsPerPage = !!params && params.hasOwnProperty('itemsPerPage') ? params.itemsPerPage : self.paginator.itemsPerPageOptions.first();
      });
    });
  }

  angular.module('app.controllers')
    .factory('baseController', function() {
      return baseController;
    });
}());