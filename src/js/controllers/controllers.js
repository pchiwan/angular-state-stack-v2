angular.module('app.controllers')
  .controller('usersController', function($scope, $rootScope, $stateParams, $injector, baseController) {

    baseController.call(this, $scope, $rootScope, $stateParams, $injector);

    var jsonpService = $injector.get('jsonPlaceholderService');

    this.title = 'Users';
    this.loadMethod = jsonpService.getUsers;

    this.load();
  })
  .controller('albumsController', function($scope, $rootScope, $stateParams, $injector, baseController) {

    baseController.call(this, $scope, $rootScope, $stateParams, $injector);

    var jsonpService = $injector.get('jsonPlaceholderService');;

    this.title = 'Albums';
    this.userId = $stateParams.userId;
    this.loadMethod = !!this.userId ? jsonpService.getAlbumsByUser : jsonpService.getAlbums;

    this.load(!!this.userId ? parseInt(this.userId, 10) : null);
  })
  .controller('todosController', function($scope, $rootScope, $stateParams, $injector, baseController) {

    baseController.call(this, $scope, $rootScope, $stateParams, $injector);

    var jsonpService = $injector.get('jsonPlaceholderService');;

    this.title = 'Todos';
    this.userId = $stateParams.userId;
    this.loadMethod = !!this.userId ? jsonpService.getTodosByUser : jsonpService.getTodos;

    this.load(!!this.userId ? parseInt(this.userId, 10) : null);
  })
  .controller('postsController', function($scope, $rootScope, $stateParams, $injector, baseController) {

    baseController.call(this, $scope, $rootScope, $stateParams, $injector);

    var jsonpService = $injector.get('jsonPlaceholderService');

    this.title = 'Posts';
    this.userId = $stateParams.userId;
    this.loadMethod = !!this.userId ? jsonpService.getPostsByUser : jsonpService.getPosts;

    this.load(!!this.userId ? parseInt(this.userId, 10) : null);
  })
  .controller('photosController', function($scope, $rootScope, $stateParams, $injector, baseController) {

    baseController.call(this, $scope, $rootScope, $stateParams, $injector);

    var jsonpService = $injector.get('jsonPlaceholderService');

    this.title = 'Photos';    
    this.albumId = $stateParams.albumId;
    this.userId = $stateParams.userId;
    this.loadMethod = jsonpService.getPhotosByAlbum;

    this.load(parseInt(this.albumId, 10));
  })
  .controller('commentsController', function($scope, $rootScope, $stateParams, $injector, baseController) {

    baseController.call(this, $scope, $rootScope, $stateParams, $injector);

    var jsonpService = $injector.get('jsonPlaceholderService');

    this.title = 'Comments';
    this.postId = $stateParams.postId;
    this.userId = $stateParams.userId;
    this.loadMethod = jsonpService.getCommentsByPost;

    this.load(parseInt(this.postId, 10));
  })
  .controller('homeController', function($scope, $rootScope, $stateParams, $injector, baseController) {

    baseController.call(this, $scope, $rootScope, $stateParams, $injector);

    this.title = 'Home';
    this.message = 'This is the Home';
  })
  .controller('aboutController', function($scope, $rootScope, $stateParams, $injector, baseController) {

    baseController.call(this, $scope, $rootScope, $stateParams, $injector);

    this.title = 'About';
    this.message = 'This is the About';
  });