Array.prototype.any = function () {
	return this.length > 0;
};

Array.prototype.first = function () {
	if (this.length) {
		return this[0];
	}
	return null;
};

Array.prototype.last = function () {
	if (this.length) {
		return this[this.length - 1];
	}
	return null;
};

String.prototype.contains = function (searchString) {
	return this.indexOf(searchString) >= 0;
};

String.prototype.format = function () {
    ///<summary>Formats a string by replacing the numbers between {} with the passed in arguments.</summary>

    var args = arguments;
    return this.replace(/{(\d+)}/g, function (match, number) {
        return typeof args[number] != 'undefined'
            ? args[number]
            : match;
    });
};
