angular.module('app.filters')
  .filter('pagination', function () {
    return function(input, start, end) {
      return input.slice(parseInt(start, 10) - 1, parseInt(end, 10));
    };
  });