angular.module('app', ['ui.router', 'app.controllers', 'app.directives', 'app.providers', 'app.filters'])
  .config(function($stateProvider, $urlRouterProvider) {
    //
    // For any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise('/home');
    //
    // Now set up the states
    var states = [{
      name: 'home',
      url: '/home',
      templateUrl: 'src/js/partials/view.html',
      controller: 'homeController as ctrl'
    }, {
      name: 'users',
      url: '/users',
      templateUrl: 'src/js/partials/users.html',
      controller: 'usersController as ctrl'
    }, {
      name: 'users-posts',
      url: '/users/:userId/posts',
      templateUrl: 'src/js/partials/posts.html',
      controller: 'postsController as ctrl'
    }, {
      name: 'users-posts-comments',
      url: '/users/:userId/posts/:postId/comments',
      templateUrl: 'src/js/partials/comments.html',
      controller: 'commentsController as ctrl'
    }, {
      name: 'users-albums',
      url: '/users/:userId/albums',
      templateUrl: 'src/js/partials/albums.html',
      controller: 'albumsController as ctrl'
    }, {
      name: 'users-albums-photos',
      url: '/users/:userId/albums/:albumId/photos',
      templateUrl: 'src/js/partials/photos.html',
      controller: 'photosController as ctrl'
    }, {
      name: 'users-todos',
      url: '/users/:userId/todos',
      templateUrl: 'src/js/partials/todos.html',
      controller: 'todosController as ctrl'
    }, {
      name: 'albums',
      url: '/albums',
      templateUrl: 'src/js/partials/albums.html',
      controller: 'albumsController as ctrl'
    }, {
      name: 'albums-photos',
      url: '/albums/:albumId/photos',
      templateUrl: 'src/js/partials/photos.html',
      controller: 'photosController as ctrl'
    }, {
      name: 'posts',
      url: '/posts',
      templateUrl: 'src/js/partials/posts.html',
      controller: 'postsController as ctrl'
    }, {
      name: 'posts-comments',
      url: '/posts/:postId/comments',
      templateUrl: 'src/js/partials/comments.html',
      controller: 'commentsController as ctrl'
    }, {
      name: 'todos',
      url: '/todos',
      templateUrl: 'src/js/partials/todos.html',
      controller: 'todosController as ctrl'
    }, {
      name: 'about',
      url: '/about',
      templateUrl: 'src/js/partials/view.html',
      controller: 'aboutController as ctrl'
    }];

    _.each(states, function(state) {
      $stateProvider
        .state(state.name, {
          url: state.url,
          templateProvider: function($templateCache){  
            return $templateCache.get(state.templateUrl); 
          },
          controller: state.controller
        });
    });
  })
  .run(function($rootScope) {

    $rootScope.isLoading = false;
    $rootScope.navBarItems = [{
      state: 'home',
      title: 'Home'
    }, {
      state: 'users',
      title: 'Users'
    }, {
      state: 'albums',
      title: 'Albums'
    }, {
      state: 'posts',
      title: 'Posts'
    }, {
      state: 'todos',
      title: 'Todos'
    }, {
      state: 'about',
      title: 'About'
    }];
  });